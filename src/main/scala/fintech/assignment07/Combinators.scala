package fintech.assignment07

object Combinators {
  def react(ipt: String): Int =
    ipt.foldLeft("") { (acc, c) =>
      if (acc.nonEmpty) {
        val lst = acc.last
        if (((lst.toLower == c) || (lst.toUpper == c)) && (lst != c))
          acc.substring(0, acc.length - 1)
        else acc + c.toString
      }
      else
        acc + c.toString
    } length

}
