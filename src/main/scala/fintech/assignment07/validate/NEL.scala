package fintech.assignment07.validate

import scala.collection.mutable.ListBuffer

trait NEL[+T] {
  /**
   * выывать функцию f на каждом элементе
   * @param f
   */
  def foreach(f: T => Unit): Unit

  /**
   * приклеивает other к хвосту this
   * @param other
   * @tparam U
   * @return
   */
  def ap[U >: T](other: NEL[U]): NEL[U]
}

object NEL {
  def apply[T](head: T): NEL[T] = new NELImpl[T](List(head))

  // begin of solution
  private final case class NELImpl[+T] (xs: List[T]) extends NEL[T] {
    override def foreach(f: T => Unit): Unit = xs.foreach(f)

    override def ap[U >: T](other: NEL[U]): NEL[U] = {
      val buff = new ListBuffer[U]
      buff.appendAll(xs)
      other.foreach(x => buff.+=(x))
      new NELImpl[U](buff.toList)
    }
  }
}
