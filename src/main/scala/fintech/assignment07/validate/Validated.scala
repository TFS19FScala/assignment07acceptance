package fintech.assignment07.validate

import scala.collection.mutable.ListBuffer

sealed trait Validated[+E, +A] {
  /**
   * склеивает 2 validated.
   * valid + invalid    == invalid
   * valid + valid      == valid
   * invalid + valid    == invalid
   * invalid + invalid  == invalid (с конкатенацией списка ошибок)
   *
   * @param other
   * @tparam E1
   * @tparam A1
   * @return
   */
  def ap[E1 >: E, A1 >: A](other: Validated[E1, A1]): Validated[E1, A1]

  /**
   * кладет в Validated значение a, если это Valid и ничего не делает
   * если это Invalid
   * @param a
   * @tparam A1
   * @return
   */
  def pure[A1](a: A1): Validated[E, A1]

  /**
   * Превращает Validated в Either
   *
   * @return
   */
  def toEither: Either[List[E], A]
}

object Validated{
  /**
   * Создает Valid
   * @param x
   * @tparam E
   * @tparam T
   * @return
   */
  def valid[E,T](x: T): Validated[E, T] = Valid(x)

  /**
   * Создает Invalid
   * @param err
   * @tparam E
   * @tparam T
   * @return
   */
  def invalid[E,T](err: E): Validated[E, T] = Invalid(NEL.apply(err))
}

// begin of solution

case class Valid[+A](value: A) extends Validated[Nothing,A] {
  override def ap[E1 >: Nothing, A1 >: A](other: Validated[E1, A1]): Validated[E1, A1] = other match {
    case Valid(_) => other
    case Invalid(errs) => Invalid(errs)
  }

  override def toEither: Either[Nothing, A] = Right(value)

  override def pure[A1](a: A1): Validated[Nothing, A1] = Validated.valid(a)
}

case class Invalid[+E](errors: NEL[E]) extends Validated[E,Nothing] {
  override def ap[E1 >: E, A1 >: Nothing](other: Validated[E1, A1]): Validated[E1, A1] = other match {
    case Valid(_) => this
    case Invalid(others) => Invalid(errors.ap(others))
  }

  override def toEither: Either[List[E], Nothing] = {
    val buff = new ListBuffer[E]
    errors.foreach(e => buff.+=(e))
    Left(buff.toList)
  }

  override def pure[A1](a: A1): Validated[E, A1] = this
}
