package fintech.assignment07.validate

import scala.language.implicitConversions

trait Validator[+E, T] {
  def validate(in: T): Validated[E, T]
}

object Validator {
  /**
   * комбинирует несколько валидаторов вместе
   * @param vs
   * @tparam E
   * @tparam T
   * @return
   */
  def combine[E, T](vs: Validator[E, T]*): Validator[E, T] = new Validator[E, T] {
    override def validate(in: T): Validated[E, T] =
      vs
        .map(v => v.validate(in))
        .foldLeft(Validated.valid[E, T](in)) { (x, acc) => x.ap(acc) }
  }

  /**
   * создает валидатор для типа T1 из валидатора для типа T2 и функции, преобразующей T1 => T2
   * @param v
   * @param f
   * @tparam E
   * @tparam T1
   * @tparam T2
   * @return
   */
  def contramap[E, T1, T2](v: Validator[E, T2], f: T1 => T2): Validator[E, T1] = new Validator[E, T1] {
    override def validate(in: T1): Validated[E, T1] =
      v.validate(f(in)).pure(in)
  }

  /**
   * Создает range валидатор. В случае ошбки вызывает функцию onError передав туда label и неверное значение.
   * Полученный результат кладется в Invalid.
   * @param min
   * @param max
   * @param onError
   * @param label
   * @tparam E
   * @tparam T
   * @return
   */
  def range[E, T: Ordering](min: T, max: T, onError: (String, T) => E)(label: String): Validator[E, T] =
    new Validator[E, T] {
      private implicit def impOps(t: T) = implicitly[Ordering[T]].mkOrderingOps(t)

      override def validate(in: T): Validated[E, T] =
        if (min <= in && max >= in) Validated.valid(in)
        else Validated.invalid(onError(label, in))
    }

  /**
   * Создает regex валидатор. В случае ошбки вызывает функцию onError передав туда label и неверное значение.
   * Полученный результат кладется в Invalid.
   * @param regex
   * @param onError
   * @param label
   * @tparam E
   * @return
   */
  def regex[E](regex: String, onError: (String, String) => E)(label: String): Validator[E, String] =
    new Validator[E, String] {
      override def validate(in: String): Validated[E, String] =
        if (in.matches(regex)) Validated.valid(in)
        else Validated.invalid(onError(label, in))
    }
}


