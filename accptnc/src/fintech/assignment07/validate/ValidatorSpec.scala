package fintech.assignment07.validate

import fintech.assignment07.validate.Validator.{combine, contramap, range, regex}
import org.scalacheck._

class ValidatorSpec extends Properties("ValidatorSpec") {

  import Prop.forAll
  import Gen._
  import Arbitrary.arbitrary

  val smallInteger = Gen.choose(0, 100)

  val genString = Gen.alphaLowerStr

  val genTestee = for {
    foo <- smallInteger
    bar <- smallInteger
    baz <- genString
    ger <- genString
  } yield Testee2(foo, bar, baz, ger)

  implicit def arb = Arbitrary {genTestee}


  case class Error(label: String, value: String)

  val numV = range[Error, Int](1, 20, { (l, v) => Error(l, v.toString) }) _
  val strV1 = regex("""[a-m]*""", { (l, v) => Error(l, v.toString) }) _
  val strV2 = regex("""[m-z]*""", { (l, v) => Error(l, v.toString) }) _

  val testeeValidator =
    combine(
      contramap(numV("foo err"), { x: Testee2 => x.foo }),
      contramap(numV("bar err"), { x: Testee2 => x.bar }),
      contramap(strV1("baz err"), { x: Testee2 => x.baz }),
      contramap(strV2("ger err"), { x: Testee2 => x.ger })
    )

  property("validate") = forAll { (t: Testee2) =>
    val validated = testeeValidator.validate(t).toEither

    val fooErr = if ((1 to 20).contains(t.foo)) None else Some(Error("foo err", t.foo.toString))
    val barErr = if ((1 to 20).contains(t.bar)) None else Some(Error("bar err", t.bar.toString))
    val bazErr = if (t.baz.matches("""[a-m]*""")) None else Some(Error("baz err", t.baz))
    val gerErr = if (t.ger.matches("""[m-z]*""")) None else Some(Error("ger err", t.ger))

    val errs = (fooErr :: barErr :: bazErr :: gerErr :: Nil).flatten
    validated match {
      case Right(res) =>
        errs.isEmpty && t == res
      case Left(res) =>
        errs == res
    }
  }
}
