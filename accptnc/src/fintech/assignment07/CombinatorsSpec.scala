package fintech.assignment07

import org.scalacheck.{Arbitrary, Gen, Properties}
import org.scalacheck.Prop.forAll
import org.scalacheck.Arbitrary._

class CombinatorsSpec extends Properties("Combinators") {

  import Combinators._

  val genChars = for {
    n <- arbitrary[Int]
    c <- Gen.alphaLowerChar
  } yield {
    val rnd = n % 67
    if (rnd < 25)
      s"$c${c.toUpper}"
    else if (rnd > 59)
      s"${c.toUpper}$c"
    else c
  }

  val genLStr = Gen.listOfN(15000, genChars).map(_.mkString)

  property("nonEmpty sequence") = forAll(genLStr) { (a: String) =>
    react(a) == checker(a)
  }

  val genEStr = Gen.oneOf(Seq(""))
  property("Empty sequence") = forAll(genEStr) { a: String =>
    react(a) == 0
  }

  private def checker(ipt: String): Int =
    ipt.foldLeft("") { (acc, c) =>
      if (acc.nonEmpty) {
        val lst = acc.last
        if (((lst.toLower == c) || (lst.toUpper == c)) && (lst != c))
          acc.substring(0, acc.length - 1)
        else acc + c.toString
      }
      else
        acc + c.toString
    } length

}
