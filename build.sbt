import sbt.Keys.compile
import sbt.addCompilerPlugin

name := "assignment07"

version := "0.1"

scalaVersion in ThisBuild := "2.12.8"

addCompilerPlugin(scalafixSemanticdb)

libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.14.0"

lazy val Acceptance = config("acceptance").extend(Test)

lazy val solution = project.in(file(".")).settings(
  inConfig(Acceptance)(Defaults.compileSettings ++ Defaults.testSettings),
  scalaSource in Acceptance := baseDirectory.value / "src",
  unmanagedSourceDirectories in Acceptance += baseDirectory.value / "accptnc",

  compile in Acceptance := (Def.taskDyn {
    val compilation = (compile in Acceptance).value
    Def.task {
      (scalafix in Compile).toTask("").value
      compilation
    }
  }).value
)

compile in Compile := (Def.taskDyn {
  val compilation = (compile in Compile).value
  Def.task {
    (scalafix in Compile).toTask("").value
    compilation
  }
}).value
